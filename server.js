const path = require('path');
const gateway = require('express-gateway');

if (process.env.DOCKER === '1') {
  gateway()
  .load(path.join(__dirname, 'configDockerMode'))
  .run();

} else {  
  gateway()
  .load(path.join(__dirname, 'config'))
  .run();
}
